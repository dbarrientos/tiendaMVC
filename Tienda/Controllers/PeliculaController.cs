﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tienda.Models;
using PagedList;
using System.IO;

namespace Tienda.Controllers
{
    public class PeliculaController : Controller
    {
        private PeliculaDBContext db = new PeliculaDBContext();

        // GET: Pelicula
        private int paginacion = 10;
        
        public ActionResult Index(int pagina = 1)
        {
            return View(db.peliculas.ToList().ToPagedList(pagina, paginacion));
        }

        public ActionResult Lista(int pagina = 1)
        {
            if( Request.IsAjaxRequest())
            {
                return PartialView("_Resultados", db.peliculas.ToList().ToPagedList(pagina, paginacion));
            }
            return View(db.peliculas.ToList().ToPagedList(pagina, paginacion));
        }

        public ActionResult ListaImagenes(int pagina = 1)
        {
            var paginacion = 18;
            if (Request.IsAjaxRequest())
            {
                return PartialView("_ResultadosImagenes", db.peliculas.ToList().ToPagedList(pagina, paginacion));
            }
            return View(db.peliculas.ToList().ToPagedList(pagina, paginacion));
        }

        public ActionResult ListaPopOverImagenes(int pagina = 1)
        {
            var paginacion = 18;
            if (Request.IsAjaxRequest())
            {
                return PartialView("_ResultadosPopOverImagenes", db.peliculas.ToList().ToPagedList(pagina, paginacion));
            }
            return View(db.peliculas.ToList().ToPagedList(pagina, paginacion));
        }

        // GET: Pelicula/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliculaModel peliculaModel = db.peliculas.Find(id);
            if (peliculaModel == null)
            {
                return HttpNotFound();
            }
            return View(peliculaModel);
        }

        // GET: Pelicula/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pelicula/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Titulo,Fecha,Director,categoria,ruta,precio,descripcion")] PeliculaModel peliculaModel, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(
                        Server.MapPath("~/Content/imagenes"),
                        fileName);
                    file.SaveAs(path);

                    path = "/content/imagenes/" + fileName;
                    peliculaModel.ruta = path;
                }
 
                db.peliculas.Add(peliculaModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(peliculaModel);
        }

        // GET: Pelicula/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliculaModel peliculaModel = db.peliculas.Find(id);
            if (peliculaModel == null)
            {
                return HttpNotFound();
            }
            return View(peliculaModel);
        }

        // POST: Pelicula/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Titulo,Fecha,Director,categoria,ruta,precio,descripcion")] PeliculaModel peliculaModel, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(
                        Server.MapPath("~/Content/imagenes"),
                        fileName);
                    file.SaveAs(path);

                    path = "/content/imagenes/" + fileName;
                    peliculaModel.ruta = path;
                }

                db.Entry(peliculaModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(peliculaModel);
        }

        // GET: Pelicula/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PeliculaModel peliculaModel = db.peliculas.Find(id);
            if (peliculaModel == null)
            {
                return HttpNotFound();
            }
            return View(peliculaModel);
        }

        // POST: Pelicula/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PeliculaModel peliculaModel = db.peliculas.Find(id);
            db.peliculas.Remove(peliculaModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult RateProduct(int id, int rate)
        {

            int userId = 142;
            bool success = false;
            string error = "";
            try
            {
                success = CalificarPelicula(userId, id, rate);
            }
            catch (System.Exception ex)
            {
                error = ex.Message;
            }
            return Json(new { error = error, success = success, pid = id },
                JsonRequestBehavior.AllowGet);

        }

        private bool CalificarPelicula(int userId, int id, int rate)
        {
            PeliculaModel pelicula = db.peliculas.Find(id);
            if (pelicula == null)
            {
                return false;
            }
            pelicula.Rate = rate;
            db.Entry(pelicula).State = EntityState.Modified;
            db.SaveChanges();
            return true;
        }

        public ContentResult AddFavorite(int id)
        {
            List<int> favorites = Session["Favorites"] as List<int>;
            if (favorites == null)
            {
                favorites = new List<int>();
            }
            favorites.Add(id);
            Session["Favorites"] = favorites;
            return Content("The movie has been added to your favorites", "text/plain", System.Text.Encoding.Default);
        }

        public ActionResult FavoritesSlideshow()
        {
            List<PeliculaModel> favPhotos = new List<PeliculaModel>();
            List<int> favoriteIds = Session["Favorites"] as List<int>;
            if (favoriteIds == null)
            {
                favoriteIds = new List<int>();
            }
            PeliculaModel currentMovie;

            foreach (int favID in favoriteIds)
            {
                currentMovie = db.peliculas.Find(favID);
                if (currentMovie != null)
                {
                    favPhotos.Add(currentMovie);
                }
            }

            return View("SlideShow", favPhotos);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
