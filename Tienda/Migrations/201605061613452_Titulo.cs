namespace Tienda.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Titulo : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PeliculaModels", "Titulo", c => c.String(nullable: false, maxLength: 80));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PeliculaModels", "Titulo", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
