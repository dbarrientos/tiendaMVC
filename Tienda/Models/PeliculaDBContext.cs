﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Tienda.Models
{
    public class PeliculaDBContext : DbContext
    {
        public PeliculaDBContext() : base ("peliculasConnection")
        {
        }

        public DbSet<PeliculaModel> peliculas { get; set; }
        public DbSet<Comment> Comments { get; set; }
    }
}